import React from 'react';
import './Task.css';
let Task = (props)=> {
    return(
        <div className="task">
            <p><input type="text" className="ptask"  value={props.text} onChange={props.change} /><span onClick={props.remove}><button className="button">X</button></span></p>
        </div>
    )
};
       export default Task;

