import React from 'react';
import Task from "./Task";
import './Tasks.css';
let Tasks = (props)=> {
    return(
        <div>
            {props.tasks.map((task) => <Task text={task.task} remove={() => props.remove(task.id)} itemId={task.id} key={task.id} change={(event) => props.change(event, task.id)}/>)}

        </div>
    )
};
export default Tasks;