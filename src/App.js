import React, { Component } from 'react';
import './App.css';
import NewTask from "./NewTask/NewTask";
import Tasks from "./Tasks/Tasks";

class App extends Component {
    state = {
        addtasktext: ' ',
        task: [
             {task:'New films : ', id: ' '}
        ]
    };
    fullchange = (event) => {
        let addtasktext = event.target.value;
        this.setState({addtasktext})
    };
    addTask = () => {
        let addtasktext = this.state.addtasktext;
        let task = [...this.state.task];
        let newtask = {task: 'New films :' + addtasktext, id: Date.now()};
        task.push(newtask);
        this.setState({task, addtasktext: ''});
    };
    removeTask = (id) => {
        let removeTask = [...this.state.task];
        let indexTask = removeTask.findIndex(task => task.id === id );
        removeTask.splice(indexTask, 1);
        this.setState({task: removeTask});
    };
    changeTask = (event, id) => {
        let tasks= [...this.state.task];
        let index = tasks.findIndex(task =>task.id === id);
        let task = {...tasks[index]};
        task.task = event.target.value;
        tasks[index] = task;
        this.setState({task: tasks})
    };
  render() {
    return (
      <div className="App">
          <h3>IT'S IMPOSSIBLE NOT TO LOOK!</h3>
       <NewTask change = {this.fullchange} add = {this.addTask } task = {this.state.addtasktext}  />
          <Tasks tasks = {this.state.task} remove = {this.removeTask} change={this.changeTask}/>
      </div>
    );
  }
}

export default App;
